/**
 * Retorna a média dos números do array dados
 * @param {Array} dados
 */
const media = dados => {
  let retorno = 0;
  for (let index = 0; index < dados.length; index++) {
    retorno += dados[index];
  }
  return retorno / dados.length;
};

/**
 * Retorna o maior elemento do array
 * @param {Integer} dados
 */
const maior = dados => {
  let maior = 0;
  let retorno = dados.forEach(element => {
    maior = element > maior ? element : maior;
  });
  return maior;
};
