<?php 
  include ('base.php');
  
  class Pessoa extends Base {

    var $nome;
    var $idade;

    function Pessoa($n, $i) {
      parent::__construct();
      $nome = $n;
      $idade = $i;
      echo "Objeto pessoa com o nome $nome e idade $idade<br />";
    }
    
    function falarNome() {
      echo "Oá, meu nome é $this->$nome<br />";
    }
  }
?>