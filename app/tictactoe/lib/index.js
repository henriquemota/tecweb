const player = document.getElementById('player');
const tab = document.getElementById('tab');
const result = document.getElementById('result');
let jogador = 'X';

const carregarTabuleiro = () => {
  let linha = 0;
  for (let i = 0; i < 9; i++) {
    if (i != 0 && i % 3 == 0) {
      tab.innerHTML += '<br />';
      linha++;
    }
    tab.innerHTML += `<button id="${linha}-${i % 3}" onclick="processarJogada(this)">&nbsp;</button>`;
  }
};

const carregarPlayer = () => {
  player.innerText = `Jogador: ${jogador}`;
};

const processarJogada = e => {
  e.innerHTML = jogador;
  e.disabled = true;
  jogador == 'X' ? (e.style.backgroundColor = '#0000ff') : (e.style.backgroundColor = '#ff0000');
  jogador = jogador == 'X' ? '0' : 'X';
  carregarPlayer();
  let hor = checkHorizontal();
  let ver = checkVertical();
  let dia = checkDiagonal();

  if (hor != 'Empate') result.innerHTML = `Vencedor jogador ${hor}`;
  else if (ver != 'Empate') result.innerHTML = `Vencedor jogador ${ver}`;
  else if (dia != 'Empate') result.innerHTML = `Vencedor jogador ${dia}`;
};

const checkHorizontal = () => {
  let combinacao = ['', '', ''];
  let linha = -1;
  let btns = Array.from(document.getElementsByTagName('button'));
  btns.forEach(e => {
    if (e.id.split('-')[0] != linha) linha = e.id.split('-')[0]; // muda de linha
    if (['X', '0'].indexOf(e.innerHTML) >= 0) combinacao[linha] += e.innerHTML;
  });

  if (combinacao.indexOf('XXX') >= 0) return 'X';
  else if (combinacao.indexOf('000') >= 0) return '0';
  return 'Empate';
};

const checkVertical = () => {
  let combinacao = ['', '', ''];
  let coluna = -1;
  let btns = Array.from(document.getElementsByTagName('button'));
  btns.forEach(e => {
    if (e.id.split('-')[1] != coluna) coluna = e.id.split('-')[1];
    if (['X', '0'].indexOf(e.innerHTML) >= 0) combinacao[coluna] += e.innerHTML;
  });
  if (combinacao.indexOf('XXX') >= 0) return 'X';
  else if (combinacao.indexOf('000') >= 0) return '0';
  return 'Empate';
};

const checkDiagonal = () => {
  let combinacao = ['', ''];
  let btns = Array.from(document.getElementsByTagName('button'));
  btns.forEach(e => {
    if (e.id == '0-0' || e.id == '2-2') {
      if (['X', '0'].indexOf(e.innerHTML) >= 0) combinacao[0] += e.innerHTML;
    } else if (e.id == '0-2' || e.id == '2-0') {
      if (['X', '0'].indexOf(e.innerHTML) >= 0) combinacao[1] += e.innerHTML;
    } else if (e.id == '1-1') {
      if (['X', '0'].indexOf(e.innerHTML) >= 0) combinacao[0] += e.innerHTML;
      if (['X', '0'].indexOf(e.innerHTML) >= 0) combinacao[1] += e.innerHTML;
    }
  });
  if (combinacao.indexOf('XXX') >= 0) return 'X';
  else if (combinacao.indexOf('000') >= 0) return '0';
  return 'Empate';
};

// chama os metodos
carregarPlayer();
carregarTabuleiro();
