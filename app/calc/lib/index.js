let mem = 0;
let ope = '';
const display = document.getElementById('display');

const loadNumbers = () => {
  const numeros = document.getElementById('numeros');
  numeros.innerHTML = `<button onclick='clearAll()'>CE</button>`;
  numeros.innerHTML += `<button onclick='clearDisplay()'>C</button>`;
  numeros.innerHTML += `<button onclick='clearLast()'><=</button>`;
  for (let i = 0; i < 10; i++) {
    if (i % 3 == 0) numeros.innerHTML += `<br />`;
    numeros.innerHTML += `<button onclick='setValue(this)'>${9 - i}</button>`;
  }
  numeros.innerHTML += `<button onclick='setValue(this)'>,</button>`;
  numeros.innerHTML += `<button onclick='setSignal()'>+/-</button>`;

  enableKeyboard();
};

const loadOperators = () => {
  const operadores = document.getElementById('operadores');
  operadores.innerHTML = `<button onclick='setValue(this)'>+</button><br />`;
  operadores.innerHTML += `<button onclick='setValue(this)'>-</button><br />`;
  operadores.innerHTML += `<button onclick='setValue(this)'>*</button><br />`;
  operadores.innerHTML += `<button onclick='setValue(this)'>/</button><br />`;
  operadores.innerHTML += `<button onclick='setValue(this)'>=</button><br />`;
};

const clearDisplay = () => {
  display.innerHTML = '0';
};

const clearAll = () => {
  clearDisplay();
  mem = 0;
  ope = '';
};

const clearLast = () => {
  let valor = display.innerHTML.toString();
  if (valor == '0' || valor.length == 1) display.innerHTML = '0';
  else display.innerHTML = valor.substring(0, valor.length - 1);
};

const setValue = item => {
  if (item.innerHTML == ',') {
    if (display.innerHTML.indexOf(',') > 0) return;
    display.innerHTML = display.innerHTML + item.innerHTML;
  } else if ('+-*/'.indexOf(item.innerHTML) >= 0) {
    mem = parseFloat(display.innerHTML.replace(',', '.'));
    ope = item.innerHTML;
    clearDisplay();
  } else if (item.innerHTML == '=') {
    display.innerHTML = eval(mem + ope + display.innerHTML.toString().replace(',', '.'))
      .toString()
      .replace('.', ',');
    mem = 0;
    ope = '';
  } else {
    display.innerHTML == '0'
      ? (display.innerHTML = item.innerHTML)
      : (display.innerHTML = display.innerHTML + item.innerHTML);
  }
};

const setSignal = () => {
  let valor = display.innerHTML;
  valor = valor.includes('-') > 0 ? valor.replace('-', '') : (valor = '-' + valor);
  display.innerHTML = valor;
};

const enableKeyboard = () => {
  document.onkeydown = ev => {
    if ('9876543210,-+/*='.indexOf(ev.key) >= 0) setValue({ innerHTML: ev.key });
    if ('Enter' == ev.key) setValue({ innerHTML: '=' });
    if ('Escape' == ev.key) clearAll();
    if ('Delete' == ev.key) clearAll();
    if ('Backspace' == ev.key) clearLast();
  };
};

// chama os métodos
loadNumbers();
loadOperators();
