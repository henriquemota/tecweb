const loadProducts = data => {
  const select = document.getElementById('produto');
  data.map(e => {
    select.innerHTML += `<option data-id="${e.cod}" data-valor="${e.valor}" data-img="${e.img}">${e.nome}</option>`;
  });
};

const mountItem = () => {
  const produto = document.getElementById('produto');
  const qtde = document.getElementById('qtde');
  return {
    cod: produto.options[produto.selectedIndex].dataset.id,
    valor: produto.options[produto.selectedIndex].dataset.valor,
    img: produto.options[produto.selectedIndex].dataset.img,
    nome: produto.options[produto.selectedIndex].value,
    qtde: qtde.value,
    total: eval(produto.options[produto.selectedIndex].dataset.valor * qtde.value)
  };
};

const addItem = item => {
  const tabela = document.getElementById('tbody');
  tabela.innerHTML +=
    `<tr data-id="${item.cod}"><td>${item.nome}</td><td>${item.nome}</td>` +
    `<td>${item.qtde}</td><td>${item.valor}</td><td>${item.total}</td></tr>`;
};

const enableFields = enable => {
  let elems = Array.from(document.getElementsByClassName('vendaAberta'));
  elems.forEach(i => {
    if (enable) i.removeAttribute('disabled');
    else i.setAttribute('disabled', true);
  });
};
