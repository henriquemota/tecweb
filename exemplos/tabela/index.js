const PEDIDO = [
  { cod: 1, produto: 'Produto 1', qtde: 10, valor: 1 },
  { cod: 2, produto: 'Produto 2', qtde: 6, valor: 2.5 },
  { cod: 3, produto: 'Produto 3', qtde: 4, valor: 3.8 },
  { cod: 4, produto: 'Produto 4', qtde: 2, valor: 6 },
  { cod: 5, produto: 'Produto 5', qtde: 5, valor: 7.5 }
];

let populateTable = () => {
  let $body = $('tbody');
  $body.html('');
  PEDIDO.map((i, e) => {
    let temp = $body.html();
    $body.html(
      `${temp}<tr><td>${i.cod}</td><td>${i.produto}</td><td>${i.qtde}</td><td>${eval(
        i.qtde * i.valor
      )}</td><td><a href="#" onclick="removeItem(${e})">Excluir</a></td></tr>`
    );
  });
};

let calculateTotal = () => {
  let total = 0;
  PEDIDO.forEach(i => {
    total += eval(i.qtde * i.valor);
  });
  $span = $('tfoot tr td span');
  $span.html(total);
};

let removeItem = e => {
  PEDIDO.splice(e, 1);
  populateTable();
  calculateTotal();
};
