const calcularIMC = (peso, altura) => {
  return peso / Math.pow(altura / 100, 2);
};

const exibirResultado = (nome, imc) => {
  if (imc < 17) return `${nome}, você está muito abaixo do peso`;
  else if (imc >= 17 && imc <= 18.49) return `${nome}, você está abaixo do peso`;
  else if (imc > 18.49 && imc <= 24.99) return `${nome}, você está com peso normal`;
  else if (imc > 24.99 && imc <= 29.99) return `${nome}, você está acima do peso`;
  else if (imc > 29.99 && imc <= 34.99) return `${nome}, você está com obesidade tipo I`;
  else if (imc > 34.99 && imc <= 39.99) return `${nome}, você está com obesidade tipo II`;
  else return `${nome}, você está com obesidade III!`;
};
