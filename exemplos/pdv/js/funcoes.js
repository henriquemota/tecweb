const loadProdutos = () => {
  const p = $('#slproduto')
  produtos.map(i => {
    $(p).append(`<option value='${i.cod}' data-valor='${i.preco}'>${i.nome}</option>`)
  })
}

const addProduto = () => {
  const item = $('select option:selected')
  const qtde = $('#txquantidade')
  const tbody = $('#body')

  $(tbody).append(
    `<tr>
      <td>${item.html()}</td>
      <td>${qtde.val()}</td>
      <td>${eval(item.attr('data-valor') + '*' + item.val())}</td>
    </tr>`
  )
}
