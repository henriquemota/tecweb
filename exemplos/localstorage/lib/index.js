const loadData = () => {
  const data = [
    { cod: 1, nome: 'Pizaria do joao', distancia: 6.5, avaliacao: 4.5 },
    { cod: 2, nome: 'Pizaria da maria', distancia: 5.5, avaliacao: 5.0 },
    { cod: 3, nome: 'Pizaria da ana', distancia: 3.5, avaliacao: 3.5 },
    { cod: 4, nome: 'Pizaria do pedro', distancia: 6.0, avaliacao: 4.0 },
    { cod: 5, nome: 'Pizaria do marcio', distancia: 9.7, avaliacao: 5.0 }
  ];
  let temp = window.localStorage.getItem('data');
  if (temp == null) {
    console.log('carreguei os dados');
    window.localStorage.setItem('data', JSON.stringify(data));
  } else {
    console.log('carregado do storagedata');
  }
};

const getData = () => {
  let data = JSON.parse(window.localStorage.getItem('data'));
  data.forEach(e => {
    console.log(e.nome);
  });
};

document.body.onload = () => {
  loadData();
  getData();
};
